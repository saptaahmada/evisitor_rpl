<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table = 'tb_visitor';
    protected  $primaryKey = 'id_visitor';
    
    public function aksesRuangans()
    {
        return $this->hasMany('App\AksesRuangan', 'id_visitor', 'id_visitor');
    }

    public function aktifitasRuangans()
    {
        return $this->hasMany('App\AktifitasRuangan', 'id_visitor', 'id_visitor');
    }
    
}
