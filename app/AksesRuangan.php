<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AksesRuangan extends Model
{
    protected $table = 'tb_akses_ruangan';
    protected  $primaryKey = 'id_akses_ruangan';
	
	public function ruangan()
    {
    	return $this->belongsTo('App\Ruangan', 'id_ruangan', 'id_ruangan');
    }

	public function visitor()
    {
    	return $this->belongsTo('App\Visitor', 'id_visitor', 'id_visitor');
    }
}
