<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    protected $table = 'tb_ruangan';
    protected  $primaryKey = 'id_ruangan';

    public function aksesRuangans()
    {
        return $this->hasMany('App\AksesRuangan', 'id_ruangan', 'id_ruangan');
    }

	public function aktifitasVisitor()
    {
    	return $this->belongsTo('App\AktifitasVisitor', 'id_ruangan', 'id_ruangan');
    }
}
