<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Ruangan;

class RuanganController extends Controller
{
    public function index()
    {
    	return view("ruangan/index")->with([
            'ruangans'  => Ruangan::all(),
            'menu'      => 'ruangan'
        ]);
    }

    public function create()
    {
    	return view("ruangan/create")->with([
            'menu'      => 'ruangan'
        ]);
    }

    public function edit($id_ruangan)
    {
        $ruangan = Ruangan::find($id_ruangan);
        return view("ruangan/edit")->with([
            'ruangan'   => $ruangan,
            'menu'      => 'ruangan'
        ]);
    }

    public function createprocess(Request $request)
    {
    	$ruangan = new Ruangan;
    	$ruangan->nama_ruangan = $request->input('nama_ruangan');
        $ruangan->deskripsi = $request->input('deskripsi');
        $ruangan->is_locked = $request->input('is_locked');
    	$ruangan->created_at = date("Y-m-d H:i:s");

    	if($ruangan->save())
    		return redirect()->action('RuanganController@index')->with('message', '<div class="alert alert-success">insert berhasil</div>');

    	return redirect()->action('RuanganController@index')->with('message', '<div class="alert alert-danger">insert gagal</div>');
    }

    public function editprocess(Request $request)
    {
        $ruangan = Ruangan::find($request->input('id_ruangan'));
        $ruangan->nama_ruangan = $request->input('nama_ruangan');
        $ruangan->deskripsi = $request->input('deskripsi');
        $ruangan->is_locked = $request->input('is_locked');
        $ruangan->updated_at = date("Y-m-d H:i:s");

        if($ruangan->save())
            return redirect()->action('RuanganController@index')->with('message', '<div class="alert alert-success">edit berhasil</div>');

        return redirect()->action('RuanganController@index')->with('message', '<div class="alert alert-danger">edit gagal</div>');
    }

    public function lock($id_ruangan)
    {
        $ruangan = Ruangan::find($id_ruangan);
        $ruangan->is_locked = $ruangan->is_locked == 1 ? 0 : 1;
        $ruangan->updated_at = date("Y-m-d H:i:s");

        if($ruangan->save())
            return redirect()->action('RuanganController@index')->with('message', '<div class="alert alert-success">lock/unlock berhasil</div>');

        return redirect()->action('RuanganController@index')->with('message', '<div class="alert alert-danger">lock/unlock berhasil</div>');
    }

    public function delete($id_ruangan) {
        $ruangan = Ruangan::find($id_ruangan);
        if($ruangan->delete())
            return redirect()->back()->with('message', '<div class="alert alert-success">delete berhasil</div>');

        return redirect()->back()->with('message', '<div class="alert alert-danger">delete gagal</div>');
    }

}
