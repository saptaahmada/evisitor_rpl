<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Visitor;
use App\Param;
use App\Ruangan;
use App\AksesRuangan;
use Session;
use thiagoalessio\TesseractOCR\TesseractOCR;

class VisitorController extends Controller
{
    public function index()
    {
    	return view("visitor/index")->with([
            'visitors'  => Visitor::where('is_active','=','1')->get(),
            'menu'      => 'visitor'
        ]);
    }

    public function history()
    {
        return view("visitor/history")->with([
            'visitors'  => Visitor::where('is_active','=','0')->get(),
            'menu'      => 'visitor_history'
        ]);
    }

    public function create()
    {
        $ruangan = Ruangan::all();
    	return view("visitor/create")->with([
            'menu'      => 'visitor',
            'ruangans'  => $ruangan
        ]);
    }


    public function createVisitor(Request $request)
    {
        $tujuan_upload = 'ktp';
        $file = $request->file('file');
        $file->move($tujuan_upload, $file->getClientOriginalName());

        $visitor = $this->baca($file->getClientOriginalName());
        $ruangan = Ruangan::all();
        return view("visitor/create_visitor")->with([
            'ruangans'  => $ruangan,
            'visitor'   => $visitor,
        ]);
    }


    public function edit($id_visitor)
    {
        $ruangan        = Ruangan::all();
        $aksesRuangan   = AksesRuangan::where('id_visitor', '=', $id_visitor)->get();
        $visitor        = Visitor::find($id_visitor);
        return view("visitor/edit")->with([
            'visitor'       => $visitor,
            'ruangans'      => $ruangan,
            'aksesRuangan'  => $aksesRuangan,
            'menu'          => 'visitor'
        ]);
    }

    public function createprocess(Request $request)
    {
    	$visitor = new Visitor;
    	$visitor->nik = $request->input('nik');
    	$visitor->nama = $request->input('nama');
    	$visitor->ttl = $request->input('ttl');
    	$visitor->alamat = $request->input('alamat');
    	$visitor->rt_rw = $request->input('rt_rw');
    	$visitor->kel_desa = $request->input('kel_desa');
    	$visitor->kecamatan = $request->input('kecamatan');
    	$visitor->agama = $request->input('agama');
    	$visitor->status = $request->input('status');
    	$visitor->pekerjaan = $request->input('pekerjaan');
    	$visitor->kewarganegaraan = $request->input('kewarganegaraan');
    	$visitor->keperluan = $request->input('keperluan');
    	$visitor->id_rfid = $request->input('id_rfid');
        if($request->get('user') == null)
        	$visitor->pemberian_akses = Session::get('user_login')[0]->id_admin;
    	$visitor->tanggal_kunjungan = date("Y-m-d h:i:sa");
    	$visitor->save();

        $ruangans = $request->input('ruangan');
        foreach ($ruangans as $value) {
            $aksesRuangan = new AksesRuangan;
            $aksesRuangan->id_visitor  = $visitor->id_visitor;
            $aksesRuangan->id_ruangan  = $value;
            $aksesRuangan->save();
        }

        if($request->get('user') == null)
        	return redirect()->action('VisitorController@index');
        if($request->get('user') == 'visitor')
            return redirect()->action('VisitorController@uploadktp')->with('message', '<div class="alert alert-success">delete berhasil</div>');
    }

    public function editprocess(Request $request)
    {
        $visitor = Visitor::find($request->input('id_visitor'));
        $visitor->nik = $request->input('nik');
        $visitor->nama = $request->input('nama');
        $visitor->ttl = $request->input('ttl');
        $visitor->alamat = $request->input('alamat');
        $visitor->rt_rw = $request->input('rt_rw');
        $visitor->kel_desa = $request->input('kel_desa');
        $visitor->kecamatan = $request->input('kecamatan');
        $visitor->agama = $request->input('agama');
        $visitor->status = $request->input('status');
        $visitor->pekerjaan = $request->input('pekerjaan');
        $visitor->kewarganegaraan = $request->input('kewarganegaraan');
        $visitor->keperluan = $request->input('keperluan');
        $visitor->id_rfid = $request->input('id_rfid');
        $visitor->pemberian_akses = Session::get('user_login')[0]->id_admin;
        $visitor->save();

        $aksesToDel = AksesRuangan::where('id_visitor', '=', $visitor->id_visitor);
        $aksesToDel->delete();

        $ruangans = $request->input('ruangan');
        foreach ($ruangans as $value) {
            $aksesRuangan = new AksesRuangan;
            $aksesRuangan->id_visitor  = $visitor->id_visitor;
            $aksesRuangan->id_ruangan  = $value;
            $aksesRuangan->save();
        }

        return redirect()->action('VisitorController@index');
    }

    public function delete($id_visitor) {
        $visitor = Visitor::find($id_visitor);
        $visitor->is_active = '0';
        if($visitor->save())
            return redirect()->back()->with('message', '<div class="alert alert-success">delete berhasil</div>');
        return redirect()->back()->with('message', '<div class="alert alert-danger">delete gagal</div>');
    }

    public function uploadktp() {
        return view('visitor/upload_ktp');
    }

    public function uploadktpprocess(Request $request) {
        $tujuan_upload = 'ktp';
        $file = $request->file('file');
        $file->move($tujuan_upload, $file->getClientOriginalName());


    }

    public function baca($fileName){
        $arrSplit = array('nama', 'tempat/tgl lahir', 'nik', 'alamat', 'kel/desa', 'kecamatan', 'agama', 'status perkawinan', 'pekerjaan', 'kewarganegaraan', 'tempat/tg! lahir', 'rt/rw', 'rtrw');
        $param = Param::where('param1', '=' , 'folder_ktp')->get();
        $strKtp = "";
        if(count($param) > 0) {
            $strKtp = (new TesseractOCR($param[0]->param2.$fileName))->run();
        }

        $strKtp = str_replace(' : ', '', $strKtp);
        $strKtp = str_replace(' ? ', '', $strKtp);
        $strKtp = str_replace(':', '', $strKtp);

        $arrTampung = array();

        for ($i=0; $i<count($arrSplit); $i++) {
            $posTampung = null;
            $split      = $arrSplit[$i];
            $pos        = strpos(strtoupper($strKtp), strtoupper($split));
            $tmpung     = "";

            if($pos != null)
                $tmpung     = substr($strKtp, $pos + strlen($split));


            for($j=0; $j<count($arrSplit); $j++) {
                if($i==$j)
                    continue;
                $split2     = $arrSplit[$j];
                $pos2       = strpos(strtoupper($tmpung), strtoupper($split2));

                if($posTampung == null)
                    $posTampung = $pos2;
                else if($pos2 != null && $posTampung > $pos2)
                    $posTampung = $pos2;
            }

            $arrTampung[$split] = substr($tmpung, 0, $posTampung);
        }

        return $arrTampung;

    }

}
