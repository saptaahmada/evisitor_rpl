<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Visitor;
use App\Ruangan;
use App\AktifitasVisitor;

class DashboardController extends Controller
{
    public function index()
    {

        $visitors   = json_decode(json_encode(Visitor::all()), true);
        $ruangans   = json_decode(json_encode(Ruangan::all()), true);
        $aktifitas  = json_decode(json_encode(AktifitasVisitor::all()), true);
        $statVisitor['visitorLastMonth']  = $this->getStatistikVisitorDate($visitors, "-1 month");
        $statVisitor['visitorLastWeek']   = $this->getStatistikVisitorDate($visitors, "-1 week");
        $statVisitor['visitorToday']      = $this->getStatistikVisitorDate($visitors, "");
        $statVisitor['visitorInside']     = $this->getStatistikVisitorInside($visitors, 1);
        
        $statRuangan['listRuangan']         = $this->getListRuangan($ruangans);
        $statRuangan['aktifitasPerRuangan'] = $this->getAktifitasPerRuangan($ruangans, $aktifitas);
        $statRuangan['listColor']           = $this->getColors();

        return view("dashboard/index")->with([
            'statistikVisitor'  => $statVisitor,
            'statistikRuangan'  => $statRuangan,
            'menu'              => 'dashboard'
        ]);
    }

    private function getColors() {
        return array(
                "#5969ff",
                "#ff407b",
                "#25d5f2",
                "#ffc750",
                "#6EFF33",
                "#FF33E9",
                "#FF9C33",
                "#035D19",
                "#A13BF5",
                "#751313",
                "#141375",
                "#137562",
                "#7C7C7C",
                "#AEAEAE",
                "#61834F",
                "#FF8700",
                "#FF0000",
                "#61FF00",
                "#9BFFFA"
            );
    }

    private function getAktifitasPerRuangan($ruangans, $aktifitas) {
        $aktifitasPerRuangan = array();
        $i = 0;
        foreach ($ruangans as $ruangan) {
            $aktifitasPerRuangan[$i] = 0;
            foreach ($aktifitas as $value) {
                if($ruangan['id_ruangan'] == $value['id_ruangan']) {
                    $aktifitasPerRuangan[$i]++;
                }
            }
            $i++;
        }
        return $aktifitasPerRuangan;
    }

    private function getListRuangan($ruangans) {
        $listRuangan = array();
        foreach ($ruangans as $value)
            $listRuangan[] = $value['nama_ruangan'];
        return $listRuangan;
    }

    private function getStatistikVisitorDate($visitors, $paramDate) {
        $startdate  = date("Y-m-d",strtotime(date( "Y-m-d", strtotime(date("Y-m-d"))).$paramDate));
        $count = 0;
        foreach ($visitors as $item)
            if ($item['created_at'] >= $startdate)
                $count++;
        return $count;
    }
    private function getStatistikVisitorInside($visitors, $isActive) {
        $startdate  = date("Y-m-d",strtotime(date( "Y-m-d", strtotime(date("Y-m-d"))).""));

        $count = 0;
        foreach ($visitors as $item)
            if ($item['is_active'] == $isActive && $item['created_at'] >= $startdate)
                $count++;
        return $count;
    }
}
