<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\AktifitasVisitor;
use App\Ruangan;
use App\Visitor;
use App\AksesRuangan;

class AktifitasVisitorController extends Controller
{
    public function tracking($id_visitor)
    {
    	$aktifitas   = AktifitasVisitor::where('id_visitor', '=', $id_visitor)->get();

    	return view("aktifitasvisitor/index")->with([
            'aktifitasvisitors'     => $aktifitas,
            'menu'                  => 'visitor_history'
        ]);
    }

    public function simulasi()
    {
        $ruangans        = Ruangan::all();
        $visitors        = Visitor::where('is_active', '=', 1)->get();
    	return view("aktifitasvisitor/simulasi")->with([
            'ruangans'     => $ruangans,
            'visitors'     => $visitors,
            'menu'         => 'simulasi_aktifitas'
        ]);
    }

    public function simulasiprocess(Request $request)
    {
    	$aktifitas 				= new AktifitasVisitor;
    	$aktifitas->id_visitor 	= $request->input('id_visitor');
    	$aktifitas->id_ruangan 	= $request->input('id_ruangan');
    	$aktifitas->in_out 		= $request->input('in_out');
    	$aktifitas->is_success 	= 1;
    	$aktifitas->save();

    	$akses = AksesRuangan::where('id_ruangan', '=', $request->input('id_ruangan'))
    						->where('id_visitor', '=', $request->input('id_visitor'))->get();

        if(count($akses) > 0) {
        	return redirect()->back()
        		->with('message', '<div class="alert alert-success">berhasil</div>');
        }
        else {
        	$aktifitas->is_success = 0;
        	$aktifitas->save();

            return redirect()->back()
        		->with('message', '<div class="alert alert-danger">Tidak punya hak akses ke ruangan ini</div>');
        }
    }
}
