<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Session;

class LoginController extends Controller
{
    public function index()
    {
    	return view("login/index");
    }

    public function loginprocess(Request $request)
    {
    	$admin = Admin::where([
				    ['nama_admin', '=', $request->input('nama_admin')],
				    ['password', '=', md5($request->input('password'))]
				])->get();

    	if(count($admin) > 0){
            Session::push('user_login', $admin[0]);
    		return redirect()->action('DashboardController@index');
    	}
    	return redirect()->action('LoginController@index')->with('message', '<div class="alert alert-danger">Username atau Password salah</div>');
    }
}
