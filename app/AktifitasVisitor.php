<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AktifitasVisitor extends Model
{
    protected $table = 'tb_aktifitas_visitor';
    protected  $primaryKey = 'id_aktifitas';

	public function ruangan()
    {
    	return $this->belongsTo('App\Ruangan', 'id_ruangan', 'id_ruangan');
    }

	public function visitor()
    {
    	return $this->belongsTo('App\Visitor', 'id_visitor', 'id_visitor');
    }
}
