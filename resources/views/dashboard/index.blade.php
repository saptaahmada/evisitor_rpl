@extends('layouts.master')

@section('content')

    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pagehader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h3 class="mb-2">Dashboard </h3>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- pagehader  -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- metric -->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <div class="card-body bg-danger">
                        <h5 class="text-light">Visitor last month</h5>
                        <div class="metric-value d-inline-block">
                            <h1 class="mb-1 text-light">
                                {{$statistikVisitor['visitorLastMonth']}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. metric -->
            <!-- metric -->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <div class="card-body bg-primary">
                        <h5 class="text-light">Visitor last week</h5>
                        <div class="metric-value d-inline-block">
                            <h1 class="mb-1 text-light">
                                {{$statistikVisitor['visitorLastWeek']}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. metric -->
            <!-- metric -->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <div class="card-body bg-success">
                        <h5 class="text-light">Visitor today</h5>
                        <div class="metric-value d-inline-block">
                            <h1 class="mb-1 text-light">
                                {{$statistikVisitor['visitorToday']}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. metric -->
            <!-- metric -->
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <div class="card-body bg-secondary">
                        <h5 class="text-light">Visitor inside today</h5>
                        <div class="metric-value d-inline-block">
                            <h1 class="mb-1 text-light">
                                {{$statistikVisitor['visitorInside']}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. metric -->
        </div>
        <!-- ============================================================== -->
        <!-- revenue  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-8 col-lg-12 col-md-8 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Visitor this week</h5>
                    <div class="card-body">
                        <canvas id="revenue" width="400" height="150"></canvas>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end reveune  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- total sale  -->
            <!-- ============================================================== -->
            <div class="col-xl-4 col-lg-12 col-md-4 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Statistik Ruangan</h5>
                    <div class="card-body">
                        <canvas id="total-sale" width="220" height="155"></canvas>
                        <div class="chart-widget-list">
                            @foreach($statistikRuangan['listRuangan'] as $key => $value)
                            <p>
                                <span class="fa-xs mr-1 legend-title" style="color: {{$statistikRuangan['listColor'][$key]}}">
                                    <i class="fa fa-fw fa-square-full"></i>
                                </span>
                                <span class="legend-text"> {{$value}}</span>
                                <span class="float-right">
                                    {{$statistikRuangan['aktifitasPerRuangan'][$key]}}
                                </span>
                            </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end total sale  -->
            <!-- ============================================================== -->
        </div>
    </div>


<script type="text/javascript">
    
$(function() {
    "use strict";
    // ============================================================== 
    // Revenue
    // ============================================================== 
var ctx = document.getElementById('revenue').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',

    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        datasets: [{
            label: 'Current Week',
            data: [12, 19, 3, 17, 6, 3, 7],
          backgroundColor: "rgba(89, 105, 255,0.5)",
                        borderColor: "rgba(89, 105, 255,0.7)",
                        borderWidth: 2
            
        }, {
            label: 'Previous Week',
            data: [2, 29, 5, 5, 2, 3, 10],
              backgroundColor: "rgba(255, 64, 123,0.5)",
                        borderColor: "rgba(255, 64, 123,0.7)",
                        borderWidth: 2
        }]
    },
    options: {
        legend: {
            display: true,
            position: 'bottom',
            labels: {
                fontColor: '#71748d',
                fontFamily: 'Circular Std Book',
                fontSize: 14,
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return '$' + value;
                    }
                }
            }]
        },
        scales: {
            xAxes: [{
                ticks: {
                    fontSize: 14,
                    fontFamily: 'Circular Std Book',
                    fontColor: '#71748d',
                }
            }],
            yAxes: [{
                ticks: {
                    fontSize: 14,
                    fontFamily: 'Circular Std Book',
                    fontColor: '#71748d',
                }
            }]
        }
    }
});
   

var ruangans    = <?=json_encode($statistikRuangan['listRuangan'])?>;
var aktifitas   = <?=json_encode($statistikRuangan['aktifitasPerRuangan'])?>;
var colors      = <?=json_encode($statistikRuangan['listColor'])?>;
// ============================================================== 
// Total Sale
// ============================================================== 
var ctx = document.getElementById("total-sale").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    
    data: {
        labels: ruangans,
        datasets: [{
            backgroundColor: [
                "#5969ff",
                "#ff407b",
                "#25d5f2",
                "#ffc750"
            ],
            data: aktifitas
        }]
    },
    options: {
        legend: {
            display: false

        }
    }

});
     
   
jQuery('#locationmap').vectorMap({

    map: 'world_mill_en',
    backgroundColor: 'transparent',
    borderColor: '#000',
    borderOpacity: 0,
    borderWidth: 0,
    zoomOnScroll: false,
    color: '#25d5f2',
    regionStyle: {
        initial: {
            fill: "#e3eaef"
        }
    },
    markerStyle: {
        initial: {
            r: 9,
            fill: "#25d5f2",
            "fill-opacity": .9,
            stroke: "#fff",
            "stroke-width": 7,
            "stroke-opacity": .4
        },
        hover: {
            stroke: "#fff",
            "fill-opacity": 1,
            "stroke-width": 1.5
        }
    },

    markers: [{
        latLng: [40.71, -74],
        name: "New York"
    }, {
        latLng: [37.77, -122.41],
        name: "San Francisco"
    }, {
        latLng: [-33.86, 151.2],
        name: "Sydney"
    }, {
        latLng: [1.3, 103.8],
        name: "Singapore"
    }],


    hoverOpacity: null,
    normalizeFunction: 'linear',
    scaleColors: ['#25d5f2', '#25d5f2'],
    selectedColor: '#c9dfaf',
    selectedRegions: [],
    showTooltip: true,
    onRegionClick: function(element, code, region) {
        var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
        alert(message);
    }

});

});
</script>


@stop