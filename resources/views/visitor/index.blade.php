@extends('layouts.master')

@section('content')

<div class="container-fluid dashboard-content">
    
{!! session()->get('message') !!}

    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">
                            Visitor
                            <a href="visitor/create" class="btn btn-primary float-right">
                                <i class="fa fa-plus"></i> tambah
                            </a>
                        </h5>
                        <div class="card-body">
                            <table class="table table-hovered">
                                <thead>
                                    <th>NIK</th>
                                    <th>NAMA</th>
                                    <th>TTL</th>
                                    <th>ALAMAT</th>
                                    <th>KEPERLUAN</th>
                                    <th>RUANGAN</th>
                                    <th>ACTION</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($visitors as $key => $value): ?>
                                        <tr>
                                            <td>{{ $value->nik }}</td>
                                            <td>{{ $value->nama }}</td>
                                            <td>{{ $value->ttl }}</td>
                                            <td>{{ $value->alamat }}</td>
                                            <td>{{ $value->keperluan }}</td>
                                            <td>
                                                <ul>                                                
                                                @foreach($value->aksesRuangans as $akses)
                                                <li>{{$akses->ruangan->nama_ruangan}}</li>
                                                @endforeach
                                                </ul>
                                            </td>
                                            <td>
                                                <a href="visitor/delete/{{$value->id_visitor}}" class="btn btn-success">
                                                    <i class="fa fa-check"></i>
                                                </a>
                                                <a href="visitor/edit/{{$value->id_visitor}}" class="btn btn-warning">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="{{url('aktifitasvisitor/tracking/'.$value->id_visitor)}}" class="btn btn-primary">
                                                    <i class="fa fa-location-arrow"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop