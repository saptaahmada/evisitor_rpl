@extends('layouts.master')

@section('content')
<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Visitor</h5>
                        <div class="card-body">
                            <form method="post" action="createprocess">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>NIK</label>
                                            <input name="nik" placeholder="Masukkan nik anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" placeholder="Masukkan nama anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Tempat, tanggal Lahir</label>
                                            <input name="ttl" placeholder="Masukkan ttl anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" name="alamat" placeholder="Masukkan alamat anda"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>RT/RW</label>
                                            <input name="rt_rw" placeholder="Masukkan RT/RW anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Kelurahan & desa</label>
                                            <input name="kel_desa" placeholder="Masukkan Kelurahan & desa anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <input name="kecamatan" placeholder="Masukkan Kecamatan anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Agama</label>
                                            <input name="agama" placeholder="Masukkan Agama anda" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                                <option value="0">Single</option>
                                                <option value="1">Menikah</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pekerjaan</label>
                                            <input name="pekerjaan" placeholder="Masukkan Pekerjaan anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Kewarganegaraan</label>
                                            <input name="kewarganegaraan" placeholder="Masukkan Kewarganegaraan anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Keperluan</label>
                                            <input name="keperluan" placeholder="Masukkan Keperluan anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>ID RFID</label>
                                            <input name="id_rfid" placeholder="Masukkan ID RFID anda" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Ruangan</label>
                                            <select id='keep-order' name="ruangan[]" multiple='multiple'>
                                                @foreach($ruangans as $ruang)
                                                <option value='{{$ruang->id_ruangan}}'>
                                                    {{$ruang->nama_ruangan}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group float-right">
                                            <button class="btn btn-success" type="submit">
                                                <i class="fa fa-check"></i>
                                                Simpan
                                            </button>
                                            <a href="visitor" class="btn btn-danger">
                                                <i class="fa fa-times"></i>
                                                Batal
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#keep-order').multiSelect()
</script>

@stop