<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>E-Visitor</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link href="{{asset('admin/assets/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin/assets/libs/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/datepicker/tempusdominus-bootstrap-4.css')}}" />
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/inputmask/css/inputmask.css')}}" />
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/multi-select/css/multi-select.css')}}">
    
    <script src="{{asset('admin/assets/vendor/jquery/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('admin/assets/libs/js/main-js.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/inputmask/js/jquery.inputmask.bundle.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/multi-select/js/jquery.multi-select.js')}}"></script>
</head>
<body>

{!! session()->get('message') !!}
<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Visitor</h5>
                        <div class="card-body">
                            <form method="post" action="{{url('/visitor/createprocess?user=visitor')}}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>NIK</label>
                                            <input name="nik" placeholder="Masukkan nik anda" class="form-control" value="{{$visitor['nik']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input name="nama" placeholder="Masukkan nama anda" class="form-control" value="{{$visitor['nama']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Tempat, tanggal Lahir</label>
                                            <input name="ttl" placeholder="Masukkan ttl anda" class="form-control" value="{{$visitor['tempat/tgl lahir']}}{{$visitor['tempat/tg! lahir']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea class="form-control" name="alamat" placeholder="Masukkan alamat anda">{{$visitor['alamat']}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>RT/RW</label>
                                            <input name="rt_rw" placeholder="Masukkan RT/RW anda" class="form-control" value="{{$visitor['rt/rw']}} {{$visitor['rt/rw']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Kelurahan & desa</label>
                                            <input name="kel_desa" placeholder="Masukkan Kelurahan & desa anda" class="form-control" value="{{$visitor['kel/desa']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <input name="kecamatan" placeholder="Masukkan Kecamatan anda" class="form-control" value="{{$visitor['kecamatan']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Agama</label>
                                            <input name="agama" placeholder="Masukkan Agama anda" class="form-control" value="{{$visitor['agama']}}" required>
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" name="status">
                                                <option value="0">Single</option>
                                                <option value="1">Menikah</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pekerjaan</label>
                                            <input name="pekerjaan" placeholder="Masukkan Pekerjaan anda" class="form-control" value="{{$visitor['pekerjaan']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Kewarganegaraan</label>
                                            <input name="kewarganegaraan" placeholder="Masukkan Kewarganegaraan anda" class="form-control" value="{{$visitor['kewarganegaraan']}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Keperluan</label>
                                            <input name="keperluan" placeholder="Masukkan Keperluan anda" class="form-control" required>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label>ID RFID</label>
                                            <input name="id_rfid" placeholder="Masukkan ID RFID anda" class="form-control">
                                        </div> -->
                                        <div class="form-group">
                                            <label>Ruangan</label>
                                            <select id='keep-order' name="ruangan[]" multiple='multiple'>
                                                @foreach($ruangans as $ruang)
                                                <?php if($ruang->is_locked == 0) { ?>
                                                <option value='{{$ruang->id_ruangan}}'>
                                                    {{$ruang->nama_ruangan}}
                                                </option>
                                                <?php } ?>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group float-right">
                                            <button class="btn btn-success" type="submit">
                                                <i class="fa fa-check"></i>
                                                Simpan
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('#keep-order').multiSelect()
</script>
</body>
</html>