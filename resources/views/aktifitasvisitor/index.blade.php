@extends('layouts.master')

@section('content')

<div class="container-fluid dashboard-content">
    
{!! session()->get('message') !!}

    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">
                            Aktifitas Visitor
                        </h5>
                        <div class="card-body">
                            <table class="table table-hovered">
                                <thead>
                                    <th>VISITOR</th>
                                    <th>RUANGAN</th>
                                    <th>TANGGAL</th>
                                    <th>KETERANGAN</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($aktifitasvisitors as $key => $value): ?>
                                        <tr>
                                            <td>{{ $value->visitor->nama }}</td>
                                            <td>{{ $value->ruangan->nama_ruangan }}</td>
                                            <td>{{ $value->created_at }}</td>
                                            <td>{!! $value->in_out==1?'<div class="badge badge-success">masuk</div>':'<div class="badge badge-danger">keluar</div>' !!}</td>
                                            <td>{{ $value->nama_ruangan }}</td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop