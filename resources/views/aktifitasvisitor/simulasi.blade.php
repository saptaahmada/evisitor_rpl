@extends('layouts.master')

@section('content')

<div class="container-fluid dashboard-content">
{!! session()->get('message') !!}
    <div class="row">
        <div class="col-xl-6">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Simulasi</h5>
                        <div class="card-body">
                            <form method="post" action="{{url('aktifitasvisitor/simulasiprocess')}}">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-xl-12">
                                        <div class="form-group">
                                            <label>Ruangan</label>
                                            <select class="form-control" name="id_ruangan">
                                                @foreach($ruangans as $ruang)
                                                <option value='{{$ruang->id_ruangan}}'>
                                                    {{$ruang->nama_ruangan}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Visitor</label>
                                            <select class="form-control" name="id_visitor">
                                                @foreach($visitors as $visitor)
                                                <option value='{{$visitor->id_visitor}}'>
                                                    {{$visitor->nama}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Aktifitas</label>
                                            <select class="form-control" name="in_out">
                                                <option value='1'>masuk</option>
                                                <option value='0'>keluar</option>
                                            </select>
                                        </div>
                                        <div class="form-group float-right">
                                            <button class="btn btn-success" type="submit">
                                                <i class="fa fa-check"></i>
                                                Simpan
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop