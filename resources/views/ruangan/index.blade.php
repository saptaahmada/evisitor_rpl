@extends('layouts.master')

@section('content')

<div class="container-fluid dashboard-content">
    
{!! session()->get('message') !!}

    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">
                            Ruangan
                            <a href="ruangan/create" class="btn btn-primary float-right">
                                <i class="fa fa-plus"></i> tambah
                            </a>
                        </h5>
                        <div class="card-body">
                            <table class="table table-hovered">
                                <thead>
                                    <th>RUANGAN</th>
                                    <th>DESKRIPSI</th>
                                    <th>IS LOCKED</th>
                                    <th style="width: 200px">ACTION</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($ruangans as $key => $value): ?>
                                        <tr>
                                            <td>{{ $value->nama_ruangan }}</td>
                                            <td>{{ $value->deskripsi }}</td>
                                            <td>{{ $value->is_locked == 0 ? 'Open':'Locked' }}</td>
                                            <td>
                                                <a href="ruangan/delete/{{$value->id_ruangan}}" class="btn btn-danger">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                <a href="ruangan/edit/{{$value->id_ruangan}}" class="btn btn-warning">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="ruangan/lock/{{$value->id_ruangan}}" class="btn btn-primary">
                                                    @if($value->is_locked == 0)
                                                        <i class="fa fa-lock"></i>
                                                    @else
                                                        <i class="fa fa-unlock"></i>
                                                    @endif
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop