@extends('layouts.master')

@section('content')
<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Ruangan</h5>
                        <div class="card-body">
                            <form method="post" action="createprocess">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Nama Ruangan</label>
                                            <input name="nama_ruangan" placeholder="Masukkan nama ruangan" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Is Locked</label>
                                            <select class="form-control" name="is_locked">
                                                <option value="0">Open</option>
                                                <option value="1">Locked</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="form-group">
                                            <label>Deskripsi Ruangan</label>
                                            <textarea name="deskripsi" placeholder="Masukkan Deskripsi tentang ruangan" class="form-control"></textarea>
                                        </div>
                                        <div class="form-group float-right">
                                            <button class="btn btn-success" type="submit">
                                                <i class="fa fa-check"></i>
                                                Simpan
                                            </button>
                                            <a href="ruangan" class="btn btn-danger">
                                                <i class="fa fa-times"></i>
                                                Batal
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop