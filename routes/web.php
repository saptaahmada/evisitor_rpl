<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@index');
Route::post('/login/loginprocess', 'LoginController@loginprocess');



Route::get('/ruangan', 'RuanganController@index');
Route::get('/ruangan/create', 'RuanganController@create');
Route::post('/ruangan/createprocess', 'RuanganController@createprocess');
Route::get('/ruangan/edit/{id_ruangan}', 'RuanganController@edit');
Route::post('/ruangan/editprocess', 'RuanganController@editprocess');
Route::get('/ruangan/delete/{id_ruangan}', 'RuanganController@delete');
Route::get('/ruangan/lock/{id_ruangan}', 'RuanganController@lock');



Route::get('/', 'LoginController@index');
Route::get('/visitor', 'VisitorController@index');
Route::get('/visitor/history', 'VisitorController@history');
Route::get('/visitor/create', 'VisitorController@create');
Route::post('/visitor/createprocess', 'VisitorController@createprocess');
Route::get('/visitor/edit/{id_visitor}', 'VisitorController@edit');
Route::post('/visitor/editprocess', 'VisitorController@editprocess');
Route::get('/visitor/delete/{id_visitor}', 'VisitorController@delete');

Route::get('/visitor/baca', 'VisitorController@baca');
Route::get('/uploadktp', 'VisitorController@uploadktp');
// Route::post('/uploadktp/proses', 'VisitorController@uploadktpprocess');
Route::post('/createvisitor', 'VisitorController@createVisitor');



Route::get('/aktifitasvisitor/tracking/{id_visitor}', 'AktifitasVisitorController@tracking');
Route::get('/aktifitasvisitor/simulasi', 'AktifitasVisitorController@simulasi');
Route::post('/aktifitasvisitor/simulasiprocess', 'AktifitasVisitorController@simulasiprocess');



Route::get('/dashboard', 'DashboardController@index');